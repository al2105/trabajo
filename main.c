#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define V 20   // indicar la longitud del campo
#define H 50   //indicar la lonngitud del campo
#define N 100 // longitud del cuerpo de la serpiente
typedef struct{
int x, y;
int ModX, ModY;
char imagen;
}snk;
typedef struct{
  int x, y;
}frt;
snk snake[N];          // (vector de estructura) de que tan grande sera la serpiente
frt fruta;
void inicio(int *tam, char campo[V][H]);
void intro_Campo(char campo[V][H]);
void intro_Datos(char campo[V][H], int tam);
void loop(char campo[V][H], int tam);
void input(char campo[V][H], int *tam, int *muerto);
void update(char campo[V][H], int tam );
void intro_Datos2(char campo[V][H], int tam);


int main()
{



    system("COLOR 20");

    int tam;
   char campo[V][H]; // matriz que va a recoger todos los elementos









    int n, opcion;


        do
    {
        printf( "\n   1._ Iniciar juego", 163 );
        printf( "\n   2._ Como se juega", 163 );
        printf( "\n   3._ Agradecimientos", 163 );
        printf( "\n   Presiona 4 para salir." );
        printf( "\n\n   Introduzca opcion (1-4): ", 162 );

        scanf( "%d", &opcion );



        switch ( opcion ){

            case 1:
inicio(&tam,campo); // iniciar todos los elementos
    //draw(campo);
    loop(campo, tam);
    system("pause");

                    break;

            case 2:
                printf("con la tecla w te desplazas hacia arriba\n");
                printf("con la tecla s te desplazas hacia abajo\n");
                printf("con la tecla a te desplazas hacia izquierda\n");
                printf("con la tecla d te desplazas hacia derecha\n");
                    break;

            case 3:

            printf(" Quiero agradecer a los grandicimos tutoriales de el canal:\n");
            printf(" Empieza aprogramar en youtube\n");
            printf("https://www.youtube.com/channel/UCLchXzm5K44lsprjAfnyvGQ\n");
            printf(" A mi profesor de programacion por explicarnos los procesos de la mejor manera posible\n");
            printf(" En fin disfruta el juego!!!\n");

            break;   }
    } while ( opcion != 4 );

    return 0;


}
void inicio(int *tam, char campo[V][H]){
    int i;

     snake[0].x = 32;       //indicar la posicion de la cabeza de la serpiente en x
    snake [0].y = 10;       // //indicar la posicion de la cabeza de la serpiente en x

    *tam = 4;           //tama�o con el cual iniciara la serpiente

    srand(time(NULL));      //srand generara la fruta aleatoriamente las coordenadas de la fruta en el campo

    fruta.x =rand() % (H - 1);
    fruta.y =rand() % (V - 1);

    while(fruta.x == 0){           // este es para controlar que la frut no aparesca en el borde de inicio
       fruta.x = rand() % (H - 1);
    }
    while(fruta.y == 0){
        fruta.y = rand() % (V - 1);
    }

    for(i=0;i<*tam;i++){
         // avance vertical 1 en y, horizontal 1 en x, si el avance quieres que sea diagonal sera un uno en ambos
        snake[i].ModX= 1;
        snake[i].ModY= 0;
    }

intro_Campo(campo);
intro_Datos(campo,*tam );

}

 // crear el campo de juego
 void intro_Campo(char campo[V][H]){
     int i, j;

    for (i=0;i<V;i++){
        for(j=0;j<H;j++){
            if(i == 0 || i == V - 1){
                campo[i][j] = '-';
            }
            else if (j == 0 || j == H - 1){
                campo[i][j] = '|';
            }
            else{
                campo[i][j] = ' ';
            }
        }
    }
 }//poner todos los datos dentro de la matriz campo
 void intro_Datos(char campo[V][H], int tam){
    int i;
    for( i = 1 ; i < tam ; i++ ){
       snake[i].x = snake[i-1].x-1;

        snake[i].y =snake[i-1].y;

        snake[i].imagen = 'X';// dentro del cuerpo de la serpiente
    }
        snake[0].imagen = 'O';

            for(i=0;i<tam;i++){
            campo[snake[i].y][snake[i].x]=snake[i].imagen;
        }
        campo[fruta.y][fruta.x]='%';
 }
 void draw (char campo[V][H]){
    int i, j;
        for(i=0;i<V;i++){
                for(j=0;j<H;j++){
                    printf("%c",campo[i][j]);

                }
           printf("\n");
 }
 }
 void loop(char campo[V][H], int tam){
    int muerto;
    muerto = 0;
    do{
        system("cls");
        draw(campo);
        input(campo,&tam,&muerto);
        update(campo,tam);
    } while (muerto == 0);
 }
 void input(char campo[V][H], int *tam, int *muerto){
        int i;
        char key;
        //comprobacion de muerte
        if(snake[0].x == 0 || snake[0].x == H - 1 || snake[0].y == 0 || snake[0].y == V - 1){
            *muerto = 1;
        }
        for(i=1;i<*tam;i++){
            if(snake[0].x == snake[i].x && snake[0].y == snake[i].y){   //si la coordenada en x del cuerpo de la serpiente es igual a la misma coordenada del cuerpo eso significara que mueres (tanto en x como en y)
                *muerto = 1;
            }
        }
        //comprobacion de la comicion de la fruta xd
        if(snake[0].x == fruta.x && snake[0].y == fruta.y){
            *tam += 1;
            snake[*tam-1].imagen = 'X';

            fruta.x =rand() % (H - 1);
            fruta.y =rand() % (V - 1);

            while(fruta.x == 0){           // este es para controlar que la frut no aparesca en el borde de inicio
               fruta.x = rand() % (H - 1);
            }
            while(fruta.y == 0){
                fruta.y = rand() % (V - 1);
            }
        }
        if(*muerto == 0){
            if(kbhit() == 1){
                key = getch();

                if(key == 's' && snake[0].ModY!=-1){
                    snake[0].ModX = 0;
                    snake[0].ModY = 1;
                }
                if(key =='w' && snake[0].ModY!=1){
                   snake[0].ModX = 0;
                   snake[0].ModY = -1;
                }
                if(key =='a' && snake[0].ModX!=1){
                   snake[0].ModX = -1;
                   snake[0].ModY = 0;
                }
                if(key =='d' && snake[0].ModX!=-1){
                   snake[0].ModX = 1;
                   snake[0].ModY = 0;
                }
            }
        }

 }
 void update(char campo[V][H], int tam ){
 //borrar los datos de la matriz
        intro_Campo(campo);
        // introducir datos nuevos
        intro_Datos2(campo,tam);
 }

 void intro_Datos2(char campo[V][H], int tam){
 int i;
 //crear el seguimiento de la serpiente
   for(i= tam-1; i>0; i--){
     snake[i].x = snake [i-1].x;
     snake[i].y = snake [i-1].y;
   }
   snake[0].x += snake[0].ModX;
   snake[0].y += snake[0].ModY;
   for(i=0;i<tam;i++){
      campo[snake[i].y][snake[i].x]= snake[i].imagen;
   }

   campo[fruta.y][fruta.x]= '%';
 }
